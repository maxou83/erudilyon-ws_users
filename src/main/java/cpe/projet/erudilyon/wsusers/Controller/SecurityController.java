package cpe.projet.erudilyon.wsusers.Controller;

import cpe.projet.erudilyon.wsusers.Configuration.SecurityModel;
import cpe.projet.erudilyon.wsusers.Model.User;
import cpe.projet.erudilyon.wsusers.Service.UserSecurityService;
import cpe.projet.erudilyon.wsusers.Service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@Api(value = "API de Gestion de la Sécurité")
public class SecurityController {

    private final UserSecurityService userSecurityService;

    public SecurityController(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @ApiOperation(value = "Permet de controler la disponibilité du service", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Le service est en ligne"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "La ressource demandé n'a pas été trouvé")
    })
    @GetMapping("/up")
    public String health() {
        return "UP!";
    }

    @ApiOperation(value = "Permet de récupérer les informations de connection technique ", response = SecurityModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Les informations ont été retourés"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la récupération des informations")
    })
    @GetMapping("/credential/{userId}")
    public ResponseEntity<SecurityModel> getCredentials(@PathVariable String userId) {
        SecurityModel model = userSecurityService.recupererInfosSecurite(userId);

        if (model != null)
            return new ResponseEntity<>(model, HttpStatus.OK);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
