package cpe.projet.erudilyon.wsusers.Controller;

import Model.ActivityUserTransactionDTO;
import Model.UserDTO;
import Validation.DataValidation;
import cpe.projet.erudilyon.wsusers.Model.User;
import cpe.projet.erudilyon.wsusers.Service.UserSecurityService;
import cpe.projet.erudilyon.wsusers.Service.UserService;
import cpe.projet.erudilyon.wsusers.dto.UserProfilDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin
@Api(value = "API de gestion des utilisateurs")
public class UserController {

    private final UserSecurityService userSecurityService;
    @Autowired
    private UserService userService;

    public UserController(UserService userService, UserSecurityService userSecurityService) {
        this.userService = userService;
        this.userSecurityService = userSecurityService;
    }

    /**
     * @return Une liste de tous les utilisateurs
     */
    @ApiOperation(value = "Permet de récupérer tous les utilisateurs", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Tous les utilisateurs ont été retournés"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la récupération des utilisateurs")
    })
    @GetMapping("/all")
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    /**
     * @return Une liste de tous les utilisateurs
     */
    @ApiOperation(value = "Permet de récupérer un utiliateur", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur a été retourné"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la récupération de l'utilisateur")
    })
    @GetMapping("/get/{userAzureId}")
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    public UserDTO getUser(@PathVariable String userAzureId) {
        return userService.getUser(userAzureId);
    }


    @ApiOperation(value = "Permet d'ajouter un nouvel utilisateur", response = UserDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "L'utilisateur a été ajouté avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de l'ajout de l'utilisateur")
    })
    @PostMapping("/add")
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    public ResponseEntity addUser(@RequestBody UserDTO user) {

        if (user == null)
            return ResponseEntity.badRequest().build();

        userService.addUser(user);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(user.getAzureId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Permet de modifier un nouvel utilisateur", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur a été modifié avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la modification de l'utilisateur")
    })
    @PutMapping("/edit")
    @PostAuthorize("hasAnyRole('USER', 'ADMIN')")
    public DataValidation editUser(@RequestBody UserDTO user) {

        if (user == null)
            return new DataValidation(User.class.getName(), false);

        userService.editUser(user);

        return new DataValidation(User.class.getName(), true);
    }

    @ApiOperation(value = "Permet de modifier un nouvel utilisateur", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur a été modifié avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la modification de l'utilisateur")
    })
    @PostMapping("/addSolde")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation addSolde(@RequestBody ActivityUserTransactionDTO activityUserTransactionDTO) {

        if (activityUserTransactionDTO == null)
            return new DataValidation(ActivityUserTransactionDTO.class.getName(), false);

        userService.addSolde(activityUserTransactionDTO.getUserId(), activityUserTransactionDTO.getAmount());

        return new DataValidation(User.class.getName(), true);
    }

    @ApiOperation(value = "Permet de modifier un nouvel utilisateur", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur a été modifié avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la modification de l'utilisateur")
    })
    @PutMapping("/substractSolde")
    @PostAuthorize("hasAnyRole('ADMIN','PARTNER')")
    public DataValidation substractSolde(@RequestBody ActivityUserTransactionDTO activityUserTransactionDTO) {

        if (activityUserTransactionDTO == null)
            return new DataValidation(ActivityUserTransactionDTO.class.getName(), false);

        userService.substractSolde(activityUserTransactionDTO.getUserId(), activityUserTransactionDTO.getAmount());

        return new DataValidation(User.class.getName(), true);
    }

    /**
     * @return les informations de profil d'un utilisateur
     */
    @ApiOperation(value = "Permet de récupérer le profil d'un utiliateur", response = UserProfilDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur a été retourné"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à accéder à cette ressource"),
            @ApiResponse(code = 403, message = "L'accès à cette ressource est interdit"),
            @ApiResponse(code = 404, message = "Erreur lors de la récupération de l'utilisateur")
    })
    @GetMapping("/profil/{user}")
    @PostAuthorize("hasAnyRole('USER', 'ADMIN','PARTNER')")
    public ResponseEntity<UserProfilDto> getUserProfil(@PathVariable String user) {
        UserProfilDto dto = userService.getUserProfil(user);

        if (dto != null)
            return new ResponseEntity<>(dto, HttpStatus.OK);

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
