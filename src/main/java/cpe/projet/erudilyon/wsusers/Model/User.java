package cpe.projet.erudilyon.wsusers.Model;

import Configuration.ProfilGroup;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@ApiModel(description = "Représentation d'un utilisateur")
@Entity
@Table(name = "T_E_USER_USER")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    @ApiModelProperty(value = "Identifiant technique d'un utilisateur", required = true)
    @JsonIgnore
    private Integer id;

    //TODO: Ajouter la contrainte d'unicité sur l'AzureId
    @Column(name = "USER_AZURE_ID", nullable = false)
    @ApiModelProperty(value = "Identifiant Azure d'un utilisateur", required = true)
    private String azureId;

    @Column(name = "USER_SOLDE", nullable = false)
    @ApiModelProperty(value = "Solde d'un utilisateur", required = true)
    private BigDecimal solde;

    @Column(name = "USER_TYPE", columnDefinition = "varchar(255) default 'USER'", nullable = false)
    @ApiModelProperty(value = "Type d'un utilisateur. Défaut : USER")
    private ProfilGroup type;

    @Column(name = "USER_ACTIF", columnDefinition="boolean default true", nullable = false)
    @ApiModelProperty(value = "Etat d'un utilisateur (actif ou inactif)", required = true)
    private boolean actif;

    @Column(name = "USER_CUMUL", nullable = false)
    @ApiModelProperty(value = "Cumul de points d'un utilisateur", required = true)
    private BigDecimal cumul;

    public User() {
        this.cumul = new BigDecimal("0");
        this.solde = new BigDecimal("0");
        this.actif = true;
        this.type = ProfilGroup.USER;
    }
}
