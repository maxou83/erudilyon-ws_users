package cpe.projet.erudilyon.wsusers.Configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:azureAD.properties")
@Data
public class AzureADProperties {

    @Value("${tenant.id}")
    private String tenantId;

    @Value("${group.admin}")
    private String groupAdmin;

    @Value("${group.partner}")
    private String groupPartner;

    @Value("${group.user}")
    private String groupUser;

}
