package cpe.projet.erudilyon.wsusers.Configuration;

import Configuration.ProfilGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Représentation des informations de sécurité Basic Auth")
public class SecurityModel {

    @ApiModelProperty(value = "Identifiant Basic Auth")
    String user;
    @ApiModelProperty(value = "Mot de passe Basic Auth")
    String pass;

    public SecurityModel(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    /**
     * Constructeur à partir d'un ProfilGroup
     *
     * @param profilGroup
     */
    public SecurityModel(ProfilGroup profilGroup)
    {
        this.user = profilGroup.getGroup();
        this.pass = profilGroup.getPass();
    }
}
