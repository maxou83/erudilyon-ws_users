package cpe.projet.erudilyon.wsusers.Repository;

import cpe.projet.erudilyon.wsusers.Model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByAzureId(String azureId);
}
