package cpe.projet.erudilyon.wsusers.Service;

import Configuration.ProfilGroup;
import com.microsoft.graph.logger.DefaultLogger;
import cpe.projet.erudilyon.wsusers.AzureAD.UserAzureADService;
import cpe.projet.erudilyon.wsusers.Configuration.AzureADProperties;
import cpe.projet.erudilyon.wsusers.Configuration.SecurityModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserSecurityService {

    private final AzureADProperties azureADProperties;

    /**
     * logger
     */
    private DefaultLogger logger = new DefaultLogger();

    public UserSecurityService(AzureADProperties azureADProperties) {
        this.azureADProperties = azureADProperties;
    }

    public SecurityModel recupererInfosSecurite(String userId) {

        if (StringUtils.isBlank(userId)) {
            logger.logError("le user ID ne peut être null", null);
        }

        List<String> groups = UserAzureADService.getInstance().getGroupsOfMember(userId);

        //ADMIN
        if (groups.contains(azureADProperties.getGroupAdmin()))
            return new SecurityModel(ProfilGroup.ADMIN);

        //PARTNER
        if (groups.contains(azureADProperties.getGroupPartner()))
            return new SecurityModel(ProfilGroup.PARTNER);

        //USER
        if (groups.contains(azureADProperties.getGroupUser()))
            return new SecurityModel(ProfilGroup.USER);


        return null;
    }

}
