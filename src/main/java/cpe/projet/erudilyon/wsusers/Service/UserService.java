package cpe.projet.erudilyon.wsusers.Service;

import Model.UserDTO;
import cpe.projet.erudilyon.wsusers.AzureAD.UserAzureADService;
import cpe.projet.erudilyon.wsusers.Model.User;
import cpe.projet.erudilyon.wsusers.Repository.UserRepository;
import cpe.projet.erudilyon.wsusers.dto.UserProfilDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public UserDTO getUser(String mixedUserId) {

        User userRepo = new User();

        // Si ce n'est pas un azureUserId --> donc un userId
        if (!StringUtils.isNumeric(mixedUserId) && StringUtils.contains(mixedUserId, "-")) {
            userRepo = userRepository.findByAzureId(mixedUserId);
        } else {
            Integer convertedUserId = Integer.valueOf(mixedUserId);
            userRepo = userRepository.findById(convertedUserId).get();
        }

        if (userRepo == null)
            return null;

        com.microsoft.graph.models.extensions.User u = UserAzureADService.getInstance().getUser(userRepo.getAzureId());
        UserDTO user = new UserDTO();
        user.setUserId(userRepo.getId());
        user.setDisplayName(u.displayName);
        user.setAzureId(userRepo.getAzureId());
        user.setCumul(userRepo.getCumul());
        user.setSolde(userRepo.getSolde());
        user.setType(userRepo.getType());

        return user;
    }

    public List<UserDTO> getAllUsers() {
        List<User> userList = new ArrayList<>();
        List<UserDTO> userResultList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        for (User user : userList) {

            com.microsoft.graph.models.extensions.User u = UserAzureADService.getInstance().getUser(user.getAzureId());
            UserDTO userDTO = new UserDTO();
            userDTO.setUserId(user.getId());
            userDTO.setDisplayName(u.displayName);
            userDTO.setAzureId(user.getAzureId());
            userDTO.setCumul(user.getCumul());
            userDTO.setSolde(user.getSolde());
            userDTO.setType(user.getType());
            userResultList.add(userDTO);
        }
        return userResultList;
    }

    public void addUser(UserDTO user) {
        User userRepo = new User();
        userRepo.setAzureId(user.getAzureId());

        userRepository.save(userRepo);
    }

    public void editUser(UserDTO user) {
        User userFromDb = userRepository.findByAzureId(user.getAzureId());

        userFromDb.setCumul(user.getCumul());
        userFromDb.setSolde(user.getSolde());
        userFromDb.setType(user.getType());

        userRepository.save(userFromDb);
    }


    public void substractSolde(Integer userId, BigDecimal amount) {
        User userFromDb = userRepository.findById(userId).get();

        BigDecimal finalUserSolde = userFromDb.getSolde().subtract(amount);
        if (finalUserSolde.compareTo(new BigDecimal("0")) <= 0) {
            userFromDb.setSolde(new BigDecimal("0"));
        } else {
            userFromDb.setSolde(finalUserSolde);
        }

        userRepository.save(userFromDb);
    }


    public void addSolde(Integer userId, BigDecimal activityGain) {
        User userFromDb = userRepository.findById(userId).get();

        BigDecimal finalUserSolde = userFromDb.getSolde().add(activityGain);
        BigDecimal finalUserCumul = userFromDb.getCumul().add(activityGain);
        userFromDb.setSolde(finalUserSolde);
        userFromDb.setCumul(finalUserCumul);

        userRepository.save(userFromDb);
    }

    public UserProfilDto getUserProfil(String mixedUserId) {

        User userRepo = new User();

        // Si ce n'est pas un azureUserId --> donc un userId
        if (!StringUtils.isNumeric(mixedUserId) && StringUtils.contains(mixedUserId, "-")) {
            userRepo = userRepository.findByAzureId(mixedUserId);
        } else {
            Integer convertedUserId = Integer.valueOf(mixedUserId);
            userRepo = userRepository.findById(convertedUserId).get();
        }

        if (userRepo == null)
            return null;

        com.microsoft.graph.models.extensions.User u = UserAzureADService.getInstance().getUser(userRepo.getAzureId());

        UserProfilDto userProfilDto = new UserProfilDto();
        userProfilDto.setDisplayName(u.displayName);
        if (StringUtils.isBlank(u.mail)) {
            userProfilDto.setEmail(u.userPrincipalName);
        } else {
            userProfilDto.setEmail(u.mail);
        }

        userProfilDto.setSolde(userRepo.getSolde());
        userProfilDto.setCumul(userRepo.getCumul());

        userProfilDto.setEffectif(new Long(userRepository.count()).intValue());
        userProfilDto.setRang(getRankForUser(userRepo.getId()));
        userProfilDto.setNiveau(getLevel(userRepo.getCumul()));

        return userProfilDto;


    }

    private Integer getRankForUser(Integer userId) {
        return jdbcTemplate
                .queryForObject("SELECT RANK () OVER (ORDER BY user_cumul DESC) user_cumul_rank"
                        + " FROM t_e_user_user where user_id = ?", Integer.class, userId);
    }

    private Integer getLevel(BigDecimal cumul) {
        Integer niveau = 0;

        if (cumul.compareTo(BigDecimal.ZERO) > 0 && cumul.compareTo(new BigDecimal("500")) <= 0)
            niveau = 1;

        if (cumul.compareTo(new BigDecimal("500")) > 0 && cumul.compareTo(new BigDecimal("1000")) <= 0)
            niveau = 2;

        if (cumul.compareTo(new BigDecimal("1000")) > 0 && cumul.compareTo(new BigDecimal("1500")) <= 0)
            niveau = 3;

        if (cumul.compareTo(new BigDecimal("1500")) > 0 && cumul.compareTo(new BigDecimal("2000")) < 0)
            niveau = 4;

        return niveau;
    }

}
