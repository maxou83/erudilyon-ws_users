package cpe.projet.erudilyon.wsusers.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserProfilDto {

    String displayName;
    Integer niveau;
    BigDecimal solde;
    Integer rang;
    Integer effectif;
    String email;
    BigDecimal cumul;

}
