package cpe.projet.erudilyon.wsusers.AzureAD;

import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import com.microsoft.graph.models.extensions.IGraphServiceClient;
import com.microsoft.graph.models.extensions.User;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Permet d'établir la communication avec l'API Graph de Azure AD concernant la gestion des utilisateurs
 */
public class UserAzureADService {

    /**
     * Instance du service Singleton
     */
    private static UserAzureADService INSTANCE = new UserAzureADService();

    /**
     * Instance de l'API Graph
     */
    private IGraphServiceClient graphClient = null;

    /**
     * Instance de l'authentification
     */
    private SimpleAuthProvider authProvider = null;

    /**
     * tenant
     */
    private String tenant = null;


    /**
     * logger
     */
    private DefaultLogger logger = new DefaultLogger();

    private UserAzureADService() {
    }

    public static UserAzureADService getInstance() {
        return INSTANCE;
    }

    /**
     * Permet de récupérer le Token
     */
    private String getToken() {
        // Définition du niveau d'alerte du logger
        logger.setLoggingLevel(LoggerLevel.ERROR);

        // récupération des paramètres
        Properties oAuthProperties = new Properties();
        try {
            oAuthProperties.load(UserAzureADService.class.getResourceAsStream("/azureAD.properties"));
        } catch (IOException e) {
            System.out.println("Unable to read OAuth configuration");
            return null;
        }

        tenant = oAuthProperties.getProperty("tenant.id");
        String clientId = oAuthProperties.getProperty("client.id");
        String scope = oAuthProperties.getProperty("app.scopes");
        String secretKey = oAuthProperties.getProperty("app.secretKey");
        String grandType = oAuthProperties.getProperty("app.grandType");

        // Get an access token
        String accessToken = Authentication.geAppAccessToken(tenant, clientId, scope, secretKey, grandType);

        logger.logDebug("Access token: " + accessToken);

        return accessToken;

    }

    /**
     * Initialise le GraphClient si celui ci n'est pas définit
     */
    private void ensureGraphClient() {

        String accessToken = getToken();
        if (StringUtils.isBlank(accessToken)) {
            logger.logError("L'access Token ne peut être null", null);
            return;
        }

        // Create the auth provider
        authProvider = new SimpleAuthProvider(accessToken);

        // Build a Graph client
        graphClient = GraphServiceClient.builder()
                .authenticationProvider(authProvider)
                .logger(logger)
                .buildClient();

    }

    /**
     * Permet de récupérer l'utilisateur Azure AD à partir de son ID
     *
     * @param userId l'id de l'utilisateur Azure AD
     * @return l'User Azure AD
     */
    public User getUser(String userId) {

        if (StringUtils.isBlank(userId)) {
            logger.logError("le user ID ne peut être null", null);
        }

        ensureGraphClient();

        return graphClient
                .users(userId)
                .buildRequest()
                .get();
    }


    public List<String> getGroupsOfMember(String userId) {

        String accessToken = getToken();
        if (StringUtils.isBlank(accessToken)) {
            logger.logError("L'access Token ne peut être null", null);
            return new ArrayList<>();
        }

        if (StringUtils.isAnyBlank(userId)) {
            logger.logError("Paramètre en entrée incorrect", null);
            return new ArrayList<>();
        }


        try {

            // Construction de l'URL en POST
            HttpPost post = new HttpPost("https://graph.microsoft.com/v1.0/users/" + userId + "/getMemberGroups");

            // ajout du header
            post.setHeader("Authorization", "Bearer " + accessToken);
            post.setHeader("Content-Type", "application/json");

            // ajout du body
            post.setEntity(new StringEntity("{\"securityEnabledOnly\":true}"));

            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(post)) {

                if (response.getStatusLine().getStatusCode() == HttpStatus.NOT_FOUND.value()) // 404 NOT FOUND
                {
                    logger.logDebug("Utilisateur non trouvé, erreur 404");
                    return new ArrayList<>();
                }

                // Consommation du retour
                String result = EntityUtils.toString(response.getEntity());
                logger.logDebug(result);

                // parse du résultat et récupération du "access_token"
                Object obj = new JSONParser().parse(result);
                JSONObject jo = (JSONObject) obj;
                JSONArray ja = (JSONArray) jo.get("value");
                List<Object> listObj = Arrays.asList(ja.toArray());
                List<String> liste = new ArrayList<>();

                listObj.stream().forEach(o -> liste.add(String.valueOf(o)));

                return liste;


            } catch (IOException | ParseException e) {
                logger.logError("Anomalie lors de la récupération des groupes", e);
            }
        } catch (UnsupportedEncodingException e) {
            logger.logError("Anomalie lors de la récupération des groupes", e);
        }

        return new ArrayList<>();
    }

}
