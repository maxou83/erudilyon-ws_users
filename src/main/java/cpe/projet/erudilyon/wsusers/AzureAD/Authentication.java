package cpe.projet.erudilyon.wsusers.AzureAD;

import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Authentication
 */
class Authentication {

    /**
     * URL principal de l'autorité
     */
    private static final String AUTHORITY = "https://login.microsoftonline.com/";

    /**
     * Path
     */
    private static final String OAUTH_URL = "/oauth2/v2.0/token";
    
    private Authentication() {
    }

    /**
     * Permet de récupérer le token d'accès à l'API Graph
     *
     * @param tenant    l'identifiant de l'Active Directory Azure
     * @param clientId  l'identifiant du client (application)
     * @param scope     le périmètre des autorisations demandées
     * @param secretKey la clé secrète
     * @param grandType le type d'accèss
     * @return le token obtenu
     */
    public static String geAppAccessToken(String tenant, String clientId, String scope, String secretKey, String grandType) {
        // set logger
        DefaultLogger logger = new DefaultLogger();
        logger.setLoggingLevel(LoggerLevel.ERROR);

        if (StringUtils.isAnyBlank(tenant, clientId, scope, secretKey, grandType)) {
            logger.logError("Paramètre en entré incorrect", null);
            return null;
        }

        try {

            // Construction de l'URL en POST
            HttpPost post = new HttpPost(AUTHORITY + tenant + OAUTH_URL);

            // ajout des paramètres
            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("client_id", clientId));
            urlParameters.add(new BasicNameValuePair("scope", scope));
            urlParameters.add(new BasicNameValuePair("client_secret", secretKey));
            urlParameters.add(new BasicNameValuePair("grant_type", grandType));


            post.setEntity(new UrlEncodedFormEntity(urlParameters));


            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(post)) {

                // Consommation du retour
                String result = EntityUtils.toString(response.getEntity());
                logger.logDebug(result);

                // parse du résultat et récupération du "access_token"
                Object obj = new JSONParser().parse(result);
                JSONObject jo = (JSONObject) obj;
                String token = (String) jo.get("access_token");
                logger.logDebug(token);

                return token;


            } catch (IOException | ParseException e) {
                logger.logError("Anomalie lors de la récupération du token", e);
            }

        } catch (UnsupportedEncodingException e) {
            logger.logError("Anomalie lors de la récupération du token", e);
        }

        return null;
    }
}