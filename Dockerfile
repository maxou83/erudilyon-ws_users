FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/ws-users-0.0.2-SNAPSHOT.jar app.jar
EXPOSE 8084
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]